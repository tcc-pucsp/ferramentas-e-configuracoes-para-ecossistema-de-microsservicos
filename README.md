# Ecossistema de microsserviços

Esse projeto contém informações sobre configurações e ferramentas importantes para trabalhar no ecossistema de microsserviços. 

* Arquivo [Dockerfile](https://docs.docker.com/engine/reference/builder/) para projeto Java, [Kotlin](https://kotlinlang.org/) utilizando [framework-spring](https://spring.io/)
* Arquivo Dockerfile para projeto [Clojure](https://clojure.org/)
* Arquivo [Docker Compose](https://docs.docker.com/compose/) para instância de uma imagem de banco de dados:
    - [MongoDB](https://www.mongodb.com/)
    - [MariaDB](https://mariadb.org/)
* Arquivo Docker Compose para instância de um mensageiro:
    - [RabbitMQ](https://www.rabbitmq.com/)
    - [Kafka](https://kafka.apache.org/)
* Arquivos template para configurar um serviço no [kubernetes](https://kubernetes.io/):
    - [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
    - [Service](https://kubernetes.io/docs/concepts/services-networking/service/)
    - [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/)
    - [PersistentVolumeClaims](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims)
* Arquivo gitlab-ci para CI/CD para os projetos
    - Java/Kotlin projeto [gradlew](https://gradle.org/)
    - Clojure [?]
